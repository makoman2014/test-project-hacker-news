import React, {useEffect, useState} from 'react';
import {useAppDispatch, useAppSelector} from "../../redux";
import {newsActions} from "../../redux/actions/news.actions";
import { BiCommentDetail } from 'react-icons/bi';
import {Link} from "react-router-dom";

export type newsType ={
    id: number;
    time: number;
    by: string;
    title: string;
    url: string;
    score: number;
    kids: number[]
}
const  News = () => {
    const [news, setNews] = useState<newsType[]>([])
    const [loading, setLoading] = useState<boolean>(false)
    const dispatch = useAppDispatch();
    const {data} = useAppSelector<any>(
        (state) => state.newsReducer
    );
    useEffect(()=> {
        dispatch(newsActions.getNews())
    },[dispatch])
    const fetchData = async () => {
        setLoading(true);
        const newArray = data.slice(0, 100)
        const promises = newArray.map((id:any) => {
                return fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`).then((response) =>
                        response.json()
                )
            }
        );
        const result = await Promise.all(promises);
        if(!result) return;

        const sortByDate = result.sort((s1, s2) => {
            return s1.time - s2.time;
        })
        console.log(sortByDate.slice(0,10))
        setNews(sortByDate);
        setLoading(false);
    };

    useEffect(() => {
        if (data.length > 0 ) {
            fetchData();
            const intervalId = setInterval(() => {
                fetchData();
            }, 60000); // задержка между запросами в миллисекундах (1000 миллисекунд = 1 секунда)

            return () => clearInterval(intervalId); // очищаем интервал при размонтировании компонента
        }
    }, [data]);

    return (
        <div className="container">
            <h1>News</h1>
            {news && news.length > 0 && <button onClick={fetchData}>Update info</button>}
            {loading && <p>Loading...</p>}
            <ul className="news">
                {news && news.length > 0 ?
                    news.map(item=>
                        <li key={item.id}>
                            <Link to={`/news/${item.id}`}>
                                <h3>{item.title}</h3>
                                <p>Author: {item.by}</p>
                                <p>Rate: {item.score}</p>
                                <p className="comments"><BiCommentDetail size={20}/> {item?.kids?.length ? item?.kids.length : 0}</p>
                                <p>Date: {item.time ? new Date(item.time * 1000).toISOString().slice(0, 19).replace('T', ' ') : ''}</p>
                            </Link>
                        </li>
                    )
                    : ''}
            </ul>
        </div>
    );
}

export default News;
