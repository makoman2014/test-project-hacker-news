import React, {useEffect, useState} from 'react';
import { BiCommentDetail } from 'react-icons/bi';
import {Link, useParams} from "react-router-dom";
import {newsType} from "../news/news";

type kidsType = {
    id: number;
    time: number;
    by: string;
    text: string;
}
const  OneNews = () => {
    const [news, setNews] = useState<newsType>()
    const [kids, setKids] = useState<kidsType[]>()

    const {id} = useParams()
    const getOneNews = async (id:number)=>{
        const data = await fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`).then((response) =>
            response.json()
        )
        setNews(data)
    }
    useEffect(()=> {
        if(id){
             getOneNews(Number(id))
        }
    },[id])
    const fetchData = async (newArray:number[]) => {
        const promises = newArray.map((id:any) => {
                    return fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`).then((response) =>
                        response.json()
                    )
            }

        );
        const result = await Promise.all(promises);
        if(!result) return;
        setKids(result);
    };
    useEffect(() => {
        if (news && news.kids && news.kids?.length > 0 ) {
            fetchData(news.kids);
        }
    }, [news]);
    return (
        <div className="container">
            <h1>News</h1>
            <div className="buttons-container">
                {news && id && <button onClick={()=>getOneNews(Number(id))}>Update comments</button>}
                <Link to="/">Go back</Link>
            </div>

            {news &&
                <div className="news">
                    <h3>{news.title}</h3>
                    <p>Author: {news.by}</p>
                    <p>Rate: {news.score}</p>
                    <p>Date: {news.time ? new Date(news.time * 1000).toISOString().slice(0, 19).replace('T', ' '): ''}</p>
                    <p>Url: <a href={news.url} target="_blank">{news.url}</a></p>
                    <p className="comments"><BiCommentDetail size={20}/> {news?.kids?.length ? news?.kids.length : 0}</p>
                    {kids && kids.length > 0 &&
                        <ul>
                            {kids.map(item=>
                                <li key={item.id}>
                                    <p>Author: {item.by}</p>
                                    <p>{item.text}</p>
                                    <p>Date: {item.time ? new Date(item.time * 1000).toISOString().slice(0, 19).replace('T', ' ') : ''}</p>
                                </li>
                            )}
                       </ul>
                    }
                </div>
                }
        </div>
    );
}

export default OneNews;
