import React from 'react';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import News from "./pages/news/news"
import OneNews from "./pages/oneNews/oneNews";
function App() {
    return (
        <div className="App">
            <Provider store={store}>
                <BrowserRouter>
                    <Routes>
                        <Route path="/" element={<News />}></Route>
                        <Route path="/news/:id" element={<OneNews />}></Route>
                    </Routes>
                </BrowserRouter>
            </Provider>
        </div>
    );
}

export default App;
