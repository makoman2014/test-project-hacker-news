import { store, RootState, AppDispatch } from "./store";
import { useAppDispatch, useAppSelector } from "./hooks";

export type { RootState, AppDispatch };
export { store, useAppDispatch, useAppSelector };
