import {
  SUCCESS_GET_NEWS
} from '../actionTypes';


const initialState = {
  data: [],
  errors: ''
}

function newsReducer(state = initialState, action: any):any {
  switch (action.type) {
    case SUCCESS_GET_NEWS:
      return {
        data: action.data,
        errors: ''
      };
    default:
      return state
  }
}


export {newsReducer}
