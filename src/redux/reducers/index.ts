import { combineReducers } from 'redux';
import * as treeReducer from './news.reducer';


const rootReducer = combineReducers({
      ...treeReducer,
});

export default rootReducer;
