import {AppDispatch} from '../store';
import {
    SUCCESS_GET_NEWS,
} from '../actionTypes';
import {GetInfo} from "../services/services";

const getNews =
  () =>
    async (dispatch: AppDispatch): Promise<void> => {
      try {
        const res = await GetInfo('https://hacker-news.firebaseio.com/v0/newstories.json?print=pretty')
          if (res) {
          dispatch({type: SUCCESS_GET_NEWS, data: res, loading: false, errors: ''});
        }
      } catch (err) {
          console.log(err)
      }
    };


export const newsActions = {
    getNews
};
