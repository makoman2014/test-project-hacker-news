

export function GetInfo(url:string ) {
    const requestOptions = {
        method: 'GET',
        headers: {'Content-Type': 'application/json'},
    };
    return fetch(`${url}`, requestOptions).then(handleResponse)
}
function handleResponse(response:any) {
    return response.text().then((text:string) => {
        let data = text && (typeof text === 'string') && JSON.parse(text)
        if(response.ok){
            return Promise.resolve(data);
        }
        else  {
            return Promise.reject(data);
        }
    });
}